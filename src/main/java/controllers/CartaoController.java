package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Ciclista;

import kong.unirest.Unirest;

public class CartaoController {
    private static CartaoController cartaoController;

    public static CartaoController getCartaoController() {

        if (cartaoController == null) {

            cartaoController = new CartaoController();

        }

        return cartaoController;

    }

    @SuppressWarnings("unchecked")
    public Cartao updateCartao(Ciclista ciclista, Map<String, List<String>> queryParams) {
        Map body = new HashMap<>();
        body.put("idCiclista", ciclista.getId());
        if(queryParams.get("nomeTitular") != null)
            body.put("nomeTitular", queryParams.get("nomeTitular"));
        if(queryParams.get("numero") != null)
            body.put("numero", queryParams.get("numero"));
        if(queryParams.get("validade") != null)
            body.put("validade", queryParams.get("validade"));
        if(queryParams.get("cvv") != null)
            body.put("cvv", queryParams.get("cvv"));

        Cartao cartaoAtualizado = Unirest.post("https://bicicletario-externo.herokuapp.com/validaCartaoDeCredito/"+ciclista.getId())
                .body(body).asObject(Cartao.class).getBody();
        if(cartaoAtualizado != null) {
            ciclista.setCartao(cartaoAtualizado);
            CiclistaController.alterarDados(ciclista);
            return cartaoAtualizado;
        }else {
            return null;
        }
    }

}
