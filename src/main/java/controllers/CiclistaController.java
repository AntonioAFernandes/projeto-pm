package controllers;

import com.unirio.pm.domain.*;
import com.unirio.pm.domain.Error;
import com.unirio.pm.services.CartaoService;
import com.unirio.pm.services.CiclistaService;
import com.unirio.pm.utils.Messages;
import io.javalin.http.Context;
import com.unirio.pm.domain.CiclistaStatus;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.unirio.pm.utils.Messages.NOT_FOUND_MESSAGE;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;

public class CiclistaController {

	private CiclistaController(){}

	private static CiclistaService ciclistaService;

	public static CiclistaService getCiclistaService() {
		if (ciclistaService == null) {
			ciclistaService = new CiclistaService();
		}
		return ciclistaService;
	}

	public static void getCiclista(Context ctx) {
		String queryParam = ctx.pathParam("id");
		if (queryParam == null) {
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
			return;
		}
		Ciclista ciclista = getCiclistaService().getCiclistaById(queryParam);
		if (ciclista == null) {
			ctx.status(404).json(Error.builder().codigo("404").mensagem(NOT_FOUND_MESSAGE));
			return;
		}
		ctx.status(200).json(ciclista);
	}

	public static void postCiclista(Context ctx) {
		Ciclista novo = CiclistaController.criarCampos(ctx);
		if (novo == null) {
			ctx.status(405).json(Error.builder().codigo("405").mensagem(Messages.INVALID_DATA_MESSAGE));
		}
		boolean ciclistaCriado = getCiclistaService().createCiclista(novo);
		if (ciclistaCriado) {
			ctx.status(201).json(novo);
			getCiclistaService().enviarEmail(novo.getEmail(), "Conta criada com sucesso!");
		} else {
			ctx.status(405).json(Error.builder().codigo("405").mensagem(Messages.INVALID_DATA_MESSAGE));
		}
	}

	public static void deleteCiclista(Context ctx) {
		String queryParam = ctx.pathParam("id");
		if (queryParam == null) {
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
			return;
		}
		Ciclista ciclista = getCiclistaService().getCiclistaById(queryParam);
		if (ciclista == null) {
			ctx.status(404).json(Error.builder().codigo("404").mensagem(NOT_FOUND_MESSAGE));
			return;
		}
		else {
			ctx.status(200).json(getCiclistaService().deleteCiclista(queryParam));
		}
	}

	public static void putCiclista(Context ctx) {
		Ciclista ciclista = ctx.bodyAsClass(Ciclista.class);
		if (ciclista == null) {
			ctx.status(405).json(Error.builder().codigo("404").mensagem(Messages.INVALID_DATA_MESSAGE));
			return;
		}
		if (getCiclistaService().updateCiclista(ciclista) == null) {
			ctx.status(404).json(Error.builder().codigo("404").mensagem(NOT_FOUND_MESSAGE));
			return;
		}
		else {
			ctx.status(200).json(ciclista);
		}
	}

	public static void ativarCiclista(Context ctx) {
		String queryParam = ctx.pathParam("id");
		if (queryParam == null) {
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
			return;
		}
		Ciclista ciclista = getCiclistaService().getCiclistaById(queryParam);
		if (ciclista == null) {
			ctx.status(405).json(Error.builder().codigo("405").mensagem(Messages.INVALID_DATA_MESSAGE));
			return;
		}
		ciclista.setStatus(CiclistaStatus.ATIVO);
		getCiclistaService().updateCiclista(ciclista);
		ctx.status(200).json(ciclista.getStatus());
	}
	public static void verificaEmail(Context ctx) {
		String queryParam = ctx.pathParam("email");
		if (queryParam == null) {
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
		}
		boolean temEmail = getCiclistaService().getEmail(queryParam);
		if (temEmail) {
			ctx.status(200).json(true);
		}
		else {
			ctx.status(400).json(false);
		}
	}

	public static void getCartao(Context ctx) {
		String queryParam = ctx.pathParam("id");
		if (queryParam == null) {
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
		}
		Ciclista ciclista = getCiclistaService().getCiclistaById(queryParam);
		if (ciclista != null) {
			ctx.status(200).json(ciclista.getCartao());
		}
		else {
			ctx.status(404).json(Error.builder().codigo("422").mensagem(NOT_FOUND_MESSAGE));
		}
	}

	public static void putCartao(Context ctx) {
		String idCiclista = ctx.pathParam("idCiclista");
		Ciclista ciclista = CiclistaController.getCiclistaService().getCiclistaById(idCiclista);
		if (idCiclista == null) {
			ctx.status(405).json(Error.builder().codigo("404").mensagem(Messages.INVALID_DATA_MESSAGE));
			return;
		}
		Cartao cartaoCadastrado = CartaoService.alterarCartao(ciclista, ctx.queryParamMap());
		if (cartaoCadastrado == null) {
			ctx.status(404).json(Error.builder().codigo("404").mensagem(NOT_FOUND_MESSAGE).build());
			return;
		}
		else {
			ctx.status(200).json(cartaoCadastrado);
		}
	}

	public static void postAluguel(Context ctx) {
		InputPost input = ctx.bodyAsClass(InputPost.class);
		String idTranca = input.getId();
		String idCiclista = input.getCiclista();
		boolean integrado = integrarNaRede(idTranca, idCiclista);
		if (input == null) {
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
		}
		if (!integrado) {
			Ciclista ciclista = getCiclistaService().getCiclistaById(idCiclista);
			enviarEmail(ciclista.getEmail(), "Aluguel realizado com sucesso.");
			ciclista.setUsandoBicicleta(true);
			ciclista.setStatus(CiclistaStatus.ATIVO);
			getCiclistaService().updateCiclista(ciclista);
			Unirest.post("https://sistema-bicicletario.herokuapp.com/tranca/"+idTranca+"/status/DESTRANCAR");
			Map body = new HashMap<>();
			body.put("valor", 1);
			body.put("ciclista", ciclista);
			Unirest.post("https://bicicletario-externo.herokuapp.com/cobranca/").body(body).asJson();

			ctx.status(200).json(ciclista);
		}
		else
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
	}

	public static void postDevolucao(Context ctx) {
		InputPost input = ctx.bodyAsClass(InputPost.class);
		String idTranca = input.getId();
		String idCiclista = input.getCiclista();
		boolean retirado = retirarDaRede(idTranca, idCiclista);
		if (input == null) {
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
		}
		if (!retirado) {
			//pegar o horário atual para comparação com o horário limite
			Date horaAtual = new Date();

			//configurar horário de devolução
			Calendar calendar = Calendar.getInstance();
			Date horaLimite = new Date();
			calendar.setTime(horaLimite);
			calendar.add(HOUR_OF_DAY, 120);

			//verificar se já passou da data de devolução para realizar cobrança adicional
			while (calendar.getTime().compareTo(horaAtual) <= 0) {
				//configurar 30 minutos após horário de devolução
				calendar.add(MINUTE, 30);
				Map body = new HashMap<>();
				body.put("valor", 1);
				body.put("ciclista", idCiclista);
				Unirest.post("https://bicicletario-externo.herokuapp.com/filaCobranca/").body(body).asJson();
			}

			//devolver a bicicleta
			Ciclista c = getCiclistaService().getCiclistaById(idCiclista);
			c.setUsandoBicicleta(false);
			c.setStatus(CiclistaStatus.INATIVO);
			getCiclistaService().updateCiclista(c);
			enviarEmail(getCiclistaService().getCiclistaById(idCiclista).getEmail(), "Devolução realizada com sucesso.");
			HttpResponse res = Unirest.get("https://sistema-bicicletario.herokuapp.com/totem/"+idTranca+"/trancas").asJson();
			Map body = (Map) res.getBody();
			if (body.get("status").equals("LIVRE")) {
				Unirest.post("https://sistema-bicicletario.herokuapp.com/tranca/"+idTranca+"/status/TRANCAR");
			}

			ctx.status(200).json(c);
		}
		else
			ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
	}



	public static boolean enviarEmail(String email, String mensagem) { return getCiclistaService().enviarEmail(email, mensagem); }
	public static boolean integrarNaRede(String idTranca, String idBicicleta) { return getCiclistaService().integrarNaRede(idTranca, idBicicleta);}
	public static boolean retirarDaRede(String idTranca, String idBicicleta) { return getCiclistaService().retirarDaRede(idTranca, idBicicleta);}

	public static Ciclista alterarDados(Ciclista c) {
		return getCiclistaService().updateCiclista(c);
	}

	public static Ciclista criarCampos(Context ctx) {
		Map<String, String> build = ctx.bodyAsClass(HashMap.class);

		Cartao cartao = Cartao.builder().cvv(build.get("cvv")).nomeTitular(build.get("nomeTitular")).idCartao(build.get("idCartao")).numero(build.get("numero")).validade(build.get("validade")).build();
		return Ciclista.builder().id(String.valueOf(getCiclistaService().getAllCiclistas().size()))
				.nome(build.get("nome")).cpf(build.get("cpf")).email(build.get("email")).nascimento(build.get("nascimento"))
				.nacionalidade(build.get("nacionalidade").equals("BRASILEIRO") ? Nacionalidade.BRASILEIRO : Nacionalidade.ESTRANGEIRO).senha(build.get("senha")).status(CiclistaStatus.AGUARDANDO_CONFIRMACAO).cartao(cartao).build();
	}
}
