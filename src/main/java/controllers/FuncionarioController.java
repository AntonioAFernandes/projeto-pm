package controllers;

import com.unirio.pm.domain.Error;
import com.unirio.pm.domain.Funcionario;
import com.unirio.pm.services.CiclistaService;
import com.unirio.pm.services.FuncionarioService;
import com.unirio.pm.utils.Messages;
import io.javalin.http.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FuncionarioController {

    //FINALIZADO: NÃO MEXER MAIS. TOTALMENTE FUNCIONAL

    private FuncionarioController(){}

    private static FuncionarioService funcionarioService;

    public static FuncionarioService getFuncionarioService() {
        if (funcionarioService == null) {
            funcionarioService = new FuncionarioService();
        }
        return funcionarioService;
    }

    public static void getFuncionario(Context ctx) {
        String queryParam = ctx.pathParam("id");
        if (queryParam == null) {
            ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
            return;
        }
        Funcionario funcionario = getFuncionarioService().getFuncionarioByMatricula(queryParam);
        if (funcionario == null) {
            ctx.status(404).json(Error.builder().codigo("404").mensagem(Messages.NOT_FOUND_MESSAGE));
            return;
        }
        ctx.status(200).json(funcionario);
    }

    public static void postFuncionario(Context ctx) {
        Funcionario funcionario = ctx.bodyAsClass(Funcionario.class);
        if (funcionario == null) {
            ctx.status(405).json(Error.builder().codigo("405").mensagem(Messages.INVALID_DATA_MESSAGE));
        }
        if (getFuncionarioService().createFuncionario(funcionario)) {
            ctx.status(201).json(funcionario);
        } else {
            ctx.status(405).json(Error.builder().codigo("405").mensagem(Messages.INVALID_DATA_MESSAGE));
        }
    }

    public static void deleteFuncionario(Context ctx) {
        String queryParam = ctx.pathParam("id");
        if (queryParam == null) {
            ctx.status(422).json(Error.builder().codigo("422").mensagem(Messages.INVALID_DATA_MESSAGE));
            return;
        }
        Funcionario funcionario = getFuncionarioService().getFuncionarioByMatricula(queryParam);
        if (funcionario == null) {
            ctx.status(404).json(Error.builder().codigo("404").mensagem(Messages.NOT_FOUND_MESSAGE));
            return;
        }
        else {
            ctx.status(200).json(getFuncionarioService().deleteFuncionario(queryParam));
        }
    }

    public static void putFuncionario(Context ctx) {
        Funcionario funcionario = ctx.bodyAsClass(Funcionario.class);
        if (funcionario == null) {
            ctx.status(405).json(Error.builder().codigo("404").mensagem(Messages.INVALID_DATA_MESSAGE));
            return;
        }
        if (getFuncionarioService().updateFuncionario(funcionario) == null) {
            ctx.status(404).json(Error.builder().codigo("404").mensagem(Messages.NOT_FOUND_MESSAGE));
            return;
        }
        else {
            ctx.status(200).json(funcionario);
        }
    }

    public static void getAllFuncionarios(Context ctx) {
        ctx.status(200).json(getFuncionarioService().getAllFuncionarios());
    }
}