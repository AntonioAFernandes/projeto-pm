package controllers;

import java.util.ArrayList;
import java.util.List;

import com.unirio.pm.domain.InputPost;

public class InputPostController {
    private List<InputPost> alugueis = new ArrayList<>();
    private static InputPostController inputPostController;

    public static InputPostController getAluguelController() {
        if (inputPostController == null) {
            inputPostController = new InputPostController();
        }
        return inputPostController;
    }

    public List<InputPost> getAllAluguels() {
        return this.alugueis;
    }

    public InputPost getAluguelByBicicleta(String bike) {
        for (int i = 0; i <= this.alugueis.size() - 1; i++) {
            if (this.alugueis.get(i).getBicicleta().equals(bike)) {
                return this.alugueis.get(i);
            }
        }
        return null;
    }

    public boolean createAluguel(InputPost f) {
        return this.alugueis.add(f);
    }

    public InputPost updateAluguel(InputPost f) {
        for(int i = 0; i <= this.alugueis.size() - 1; i++) {
            if (this.alugueis.get(i).getBicicleta().equals(f.getBicicleta())) {
                return this.alugueis.set(i, f);
            }
        }
        return null;
    }
}
