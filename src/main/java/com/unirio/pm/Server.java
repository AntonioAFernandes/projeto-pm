package com.unirio.pm;

import controllers.CiclistaController;
import controllers.FuncionarioController;
import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.put;

public class Server {

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 7000;
	}

	private static final Javalin app =
        Javalin.create(config -> config.defaultContentType = "application/json")
        .routes(() -> {
			get("/ciclista/:id", CiclistaController::getCiclista);
			post("/ciclista", CiclistaController::postCiclista);
			delete("/ciclista/:id", CiclistaController::deleteCiclista);
			put("/ciclista/:id", CiclistaController::putCiclista);
			post("/ciclista/:id/ativar", CiclistaController::ativarCiclista);
			get("/ciclista/existeEmail/:email", CiclistaController::verificaEmail);
			get("/cartaoDeCredito/:id", CiclistaController::getCartao);
			put("/cartaoDeCredito/:id", CiclistaController::putCartao);
			post("/aluguel", CiclistaController::postAluguel);
			post("/devolucao", CiclistaController::postDevolucao);
			get("/funcionario/:id", FuncionarioController::getFuncionario);
			post("/funcionario", FuncionarioController::postFuncionario);
			delete("/funcionario/:id", FuncionarioController::deleteFuncionario);
			put("/funcionario/:id", FuncionarioController::putFuncionario);
			get("/funcionario", FuncionarioController::getAllFuncionarios);
           });     
	  	
	public static void start() {
		app.start(getHerokuAssignedPort());
	}

	public static void stop() {
		app.stop();
	}

	public static void main(String[] args) {
		start();
	}
}
