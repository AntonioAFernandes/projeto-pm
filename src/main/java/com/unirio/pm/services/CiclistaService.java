package com.unirio.pm.services;

import com.unirio.pm.domain.*;
import controllers.CiclistaController;
import controllers.InputPostController;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;

public class CiclistaService {
    private List<Ciclista> ciclistas = new ArrayList<>();

    public List<Ciclista> getAllCiclistas() {
        return this.ciclistas;
    }

    public Ciclista getCiclistaById(String id) {
        for(int i = 0; i <= this.ciclistas.size() - 1; i++) {
            if (this.ciclistas.get(i).getId().equals(id)) {
                return this.ciclistas.get(i);
            }
        }
        return null;
    }

    public boolean createCiclista(Ciclista c) {
        if (this.getCiclistaById(c.getId()) == null) {
            return this.ciclistas.add(c);
        } else {
            return false;
        }
    }

    public boolean deleteCiclista(String id) {
        return this.ciclistas.remove(this.getCiclistaById(id));
    }

    public Ciclista updateCiclista(Ciclista c) {
        for(int i = 0; i <= this.ciclistas.size() - 1; i++) {
            if (this.ciclistas.get(i).getId().equals(c.getId())) {
                return this.ciclistas.set(i, c);
            }
        }
        return null;
    }

    public boolean getEmail(String email) {
        for(int i = 0; i <= this.ciclistas.size() - 1; i++) {
            if (this.ciclistas.get(i).getEmail().equals(email)) {
                return true;
            }
        }
        return false;
    }

    public boolean enviarEmail(String email, String mensagem) {
        Map body = new HashMap<>();
        body.put("email", email);
        body.put("mensagem", mensagem);
        HttpResponse response = Unirest.post("https://bicicletario-externo.herokuapp.com/enviarEmail")
                .body(body).asJson();
        int status = response.getStatus();
        switch (status) {
            case 200:
                return true;
            default:
                return false;
        }
    }
    public boolean integrarNaRede(String idTranca, String idBicicleta) {
        Map body = new HashMap<>();
        body.put("idTranca", idTranca);
        body.put("idBicicleta", idBicicleta);
        HttpResponse response = Unirest.post("https://sistema-bicicletario.herokuapp.com/bicicleta/integrarNaRede").body(body).asJson();
        int status = response.getStatus();
        switch (status) {
            case 200:
                return true;
            default:
                return false;
        }
    }
    public boolean retirarDaRede(String idTranca, String idBicicleta) {
        Map body = new HashMap<>();
        body.put("idTranca", idTranca);
        body.put("idBicicleta", idBicicleta);
        HttpResponse response = Unirest.post("https://sistema-bicicletario.herokuapp.com/bicicleta/retirarDaRede").body(body).asJson();
        int status = response.getStatus();
        switch (status) {
            case 200:
                return true;
            default:
                return false;
        }
    }

    public boolean cobrarTrintaMins(Tranca b) {
        Calendar atual = Calendar.getInstance();
        Date horaAtual = new Date();
        atual.setTime(horaAtual);
        Calendar prox = Calendar.getInstance();
        Date horaFim = b.getHoraFim();
        prox.setTime(horaFim);
        if (atual.compareTo(prox) != 0) {
            prox.add(MINUTE, 30);
            Map body = new HashMap<>();
            body.put("Valor cobrado:", 30);
            HttpResponse response = Unirest.post("https://bicicletario-externo.herokuapp.com/filaCobranca").body(body).asJson();
            int status = response.getStatus();
            switch (status) {
                case 200:
                     return true;
                default:
                    return false;
            }
        }
        return false;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static InputPost aluguel(String ciclista, String trancaInicio, String idBicicleta) {
        Date dataHoraAtual = new Date();
        String data = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(dataHoraAtual);
        //realizar o aluguel
        Ciclista c = CiclistaController.getCiclistaService().getCiclistaById(ciclista);
        c.setUsandoBicicleta(true);
        c.setStatus(CiclistaStatus.ATIVO);
        Unirest.post("https://sistema-bicicletario.herokuapp.com/tranca/"+trancaInicio+"/status/DESTRANCAR");
        Map body = new HashMap<>();
        body.put("valor", 1);
        body.put("ciclista", ciclista);
        HttpResponse<JsonNode> response = Unirest.post("https://bicicletario-externo.herokuapp.com/cobranca")
                .body(body).asJson();
        JSONObject cobrancaJson = response.getBody().getObject();
        String idCobranca = cobrancaJson.getString("valor");

        InputPost aluguel = InputPost.builder().bicicleta(idBicicleta).ciclista(ciclista).trancaInicio(trancaInicio).cobranca(idCobranca).horaInicio(data).build();
        InputPostController.getAluguelController().createAluguel(aluguel);
        return aluguel;
    }

    @SuppressWarnings({ "unchecked", "rawtypes", "unused" })
    public static InputPost devolucao(String ciclista, String idBicicleta, String idTrancaFim) throws ParseException {
        //pegar o horário atual para comparação com o horário limite
        InputPost aluguel = InputPostController.getAluguelController().getAluguelByBicicleta(idBicicleta);
        Date horaInicio = new SimpleDateFormat("dd/MM/yyyy").parse(aluguel.getHoraInicio());

        //configurar horário de devolução
        Calendar calendar = Calendar.getInstance();
        Date horaAtual = new Date();
        calendar.setTime(horaInicio);
        //adc mais 2h
        calendar.add(HOUR_OF_DAY, 120);

        //verificar se já passou da data de devolução para realizar cobrança adicional
        while (calendar.getTime().compareTo(horaAtual) <= 0) {
            //configurar 30 minutos após horário de devolução
            calendar.add(MINUTE, 30);
            Map body = new HashMap<>();
            body.put("valor", 30);
            body.put("ciclista", ciclista);
            Unirest.post("https://bicicletario-externo.herokuapp.com/filaCobranca/").body(body).asJson();
        }

        //devolver a bicicleta
        Ciclista c = CiclistaController.getCiclistaService().getCiclistaById(ciclista);
        c.setUsandoBicicleta(false);
        c.setStatus(CiclistaStatus.INATIVO);
        HttpResponse res = Unirest.get("https://sistema-bicicletario.herokuapp.com/tranca/"+idTrancaFim).asJson();
        Map body = (Map) res.getBody();
        if (body.get("status").equals("LIVRE")) {
            Unirest.post("https://sistema-bicicletario.herokuapp.com/tranca/"+idTrancaFim+"/status/TRANCAR");
        }else {
            return null;
        }

        Map bodyIntegrar = new HashMap<>();
        body.put("idTranca", idTrancaFim);
        body.put("idBicicleta", idBicicleta);
        Unirest.post("https://bicicletario-externo.herokuapp.com/bicicleta/integrarNaRede")
                .body(bodyIntegrar).asJson();

        Map bodyC = new HashMap<>();
        bodyC.put("idCiclista", ciclista);
        HttpResponse<JsonNode> response = Unirest.post("https://bicicletario-externo.herokuapp.com/cobranca")
                .body(bodyC).asJson();
        JSONObject cobrancaJson = response.getBody().getObject();
        String cobrancaFinal = cobrancaJson.getString("valor");

        aluguel.setHoraFim(Calendar.getInstance().toString());
        aluguel.setTrancaFim(idTrancaFim);
        aluguel.setCobranca(cobrancaFinal);

        return aluguel;
    }
}
