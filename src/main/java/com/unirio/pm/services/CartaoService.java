package com.unirio.pm.services;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Ciclista;
import controllers.CartaoController;

import java.util.List;
import java.util.Map;

public class CartaoService {

    private CartaoService() {}

    public static Cartao alterarCartao(Ciclista ciclista, Map<String, List<String>> map) {
        return CartaoController.getCartaoController().updateCartao(ciclista, map);
    }

}
