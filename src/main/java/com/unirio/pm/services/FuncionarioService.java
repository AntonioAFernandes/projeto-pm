package com.unirio.pm.services;

import com.unirio.pm.domain.Funcionario;
import controllers.FuncionarioController;

import java.util.ArrayList;
import java.util.List;

public class FuncionarioService {

    //FINALIZADO: NÃO MEXER MAIS. TOTALMENTE FUNCIONAL

    private List<Funcionario> funcionarios = new ArrayList<>();

    public List<Funcionario> getAllFuncionarios() {
        return this.funcionarios;
    }

    public Funcionario getFuncionarioByMatricula(String matricula) {
        for (int i = 0; i <= this.funcionarios.size() - 1; i++) {
            if (this.funcionarios.get(i).getMatricula().equals(matricula)) {
                return this.funcionarios.get(i);
            }
        }
        return null;
    }

    public boolean createFuncionario(Funcionario f) {
        if (this.getFuncionarioByMatricula(f.getMatricula()) == null) {
            return this.funcionarios.add(f);
        } else {
            return false;
        }
    }

    public boolean deleteFuncionario(String matricula) {
        return this.funcionarios.remove(this.getFuncionarioByMatricula(matricula));
    }

    public Funcionario updateFuncionario(Funcionario f) {
        for(int i = 0; i <= this.funcionarios.size() - 1; i++) {
            if (this.funcionarios.get(i).getMatricula().equals(f.getMatricula())) {
                return this.funcionarios.set(i, f);
            }
        }
        return null;
    }
}
