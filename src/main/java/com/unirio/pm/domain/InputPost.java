package com.unirio.pm.domain;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class InputPost {

    @JsonProperty("id")
    private String id;

    @JsonProperty("trancaInicio")
    private String trancaInicio;

    @JsonProperty("ciclista")
    private String ciclista;

    @JsonProperty("bicicleta")
    private String bicicleta;

    @JsonProperty("cobranca")
    private String cobranca;

    @JsonProperty("horaInicio")
    private String horaInicio;

    @JsonProperty("horaFim")
    private String horaFim;

    @JsonProperty("trancaFim")
    private String trancaFim;
}
