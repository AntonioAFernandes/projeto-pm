package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)

public class Bicicleta {
    @JsonProperty("id")
    private String id;

    @JsonProperty("dataInicio")
    private Date dataInicio;

    @JsonProperty("horaFim")
    private Date horaFim;

    @JsonProperty("ciclista")
    private Ciclista ciclista;
}
