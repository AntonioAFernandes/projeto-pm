package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Cartao {
    @JsonProperty("idCartao")
    private String idCartao;

    @JsonProperty("nomeTitular")
    private String nomeTitular;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("validade")
    private String validade;

    @JsonProperty("cvv")
    private String cvv;
}
