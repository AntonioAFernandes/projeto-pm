package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Funcionario {
    @JsonProperty("matricula")
    private String matricula;

    @JsonProperty("nome")
    private String nome;

    @JsonProperty("senha")
    private String senha;

    @JsonProperty("idade")
    private int idade;

    @JsonProperty("email")
    private String email;

    @JsonProperty("funcao")
    private String funcao;

    @JsonProperty("cpf")
    private String cpf;
}
