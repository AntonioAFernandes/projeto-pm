package com.unirio.pm.domain;

import lombok.Getter;

@Getter
public enum CiclistaStatus {
    ATIVO("ATIVO"),
    INATIVO("INATIVO"),
    AGUARDANDO_CONFIRMACAO("AGUARDANDO_CONFIRMACAO");

    private String status;

    CiclistaStatus(String status) {
        this.status = status;
    }
}
