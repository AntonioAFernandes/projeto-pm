package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Tranca {
    @JsonProperty ("trancaInicio")
    private String trancaInicio;

    @JsonProperty ("trancaFim")
    private String trancaFim;

    @JsonProperty("horaInicio")
    private Date horaInicio;

    @JsonProperty("horaFim")
    private Date horaFim;

    @JsonProperty("id")
    private String id;
}
