package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class NovoCiclista {

    @JsonProperty("ciclista")
    private Ciclista ciclista;

    @JsonProperty("meioDePagamento")
    private Cartao cartao;
}
