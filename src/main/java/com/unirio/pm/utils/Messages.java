package com.unirio.pm.utils;

public class Messages {

    private Messages(){}

    public final static String NOT_FOUND_MESSAGE = "Não encontrado";
    public final static String INVALID_DATA_MESSAGE = "Dados Inválidos";
    public final static String NOT_EMAIL = "Email não enviado como parâmetro";
}
