package com.unirio.pm.controllers;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class FuncionarioControllerTest {

    @Test
    public void postFuncionarioSuccessTest() {
        HttpResponse response = Unirest.post("https://projeto-pm-unirio.herokuapp.com/funcionario?matricula=1&nome=abc&senha=123&idade=25&email=a@a.com&funcao=limpa&cpf=12345612345").asJson();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void getAllFuncionariosTest() {
        HttpResponse response = Unirest.get("https://projeto-pm-unirio.herokuapp.com/funcionario").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void getFuncionarioByMatriculaTest() {
        HttpResponse response = Unirest.get("https://projeto-pm-unirio.herokuapp.com/funcionario/1").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void deleteFuncionarioByMatriculaTest() {
        HttpResponse response = Unirest.delete("https://projeto-pm-unirio.herokuapp.com/funcionario/1").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void putFuncionario() {
        HttpResponse response = Unirest.put("https://projeto-pm-unirio.herokuapp.com/funcionario/1").asString();
        assertEquals(200, response.getStatus());
    }
}
