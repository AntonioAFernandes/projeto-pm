package com.unirio.pm.controllers;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CiclistaControllerTest {
    @Test
    public void postCiclistaSuccessTest() {
        HttpResponse response = Unirest.post("https://projeto-pm-unirio.herokuapp.com/ciclista?nome=abc&nascimento=17-07-2001&cpf=12345678912&email=b@b.com&status=AGUARDANDO_CONFIRMACAO&senha=123&nacionalidade=BRASILEIRO&cvv=727&nomeTitular=abc&validade=12-12-1212&numero=4871634783254950&idCartao=1477").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void getCartao() {
        HttpResponse response = Unirest.get("https://projeto-pm-unirio.herokuapp.com/cartaoDeCredito/0").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void getCiclistaById() {
        HttpResponse response = Unirest.get("https://projeto-pm-unirio.herokuapp.com/ciclista/0").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void ativarCiclista() {
        HttpResponse response = Unirest.post("https://projeto-pm-unirio.herokuapp.com/ciclista/0/ativar").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void verifyEmail() {
        HttpResponse response = Unirest.get("https://projeto-pm-unirio.herokuapp.com/ciclista/existeEmail/b@b.com").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void deleteCiclista() {
        HttpResponse response = Unirest.delete("https://projeto-pm-unirio.herokuapp.com/ciclista/0").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void testAluguel() {
        HttpResponse response = Unirest.post("https://projeto-pm-unirio.herokuapp.com/aluguel?id=1&ciclista=0").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void testDevolucao() {
        HttpResponse response = Unirest.post("https://projeto-pm-unirio.herokuapp.com/devolucao?id=1&ciclista=0").asString();
        assertEquals(200, response.getStatus());
    }
    @Test
    public void testPutCiclista() {
        HttpResponse response = Unirest.put("https://projeto-pm-unirio.herokuapp.com/ciclista/0").asString();
        assertEquals(200, response.getStatus());
    }
}
