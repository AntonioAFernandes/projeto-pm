package com.unirio.pm.services;

import com.unirio.pm.domain.Funcionario;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FuncionarioServiceTest {
    private FuncionarioService funcionarioService;

    @Before
    public void setup() {
        this.funcionarioService = new FuncionarioService();
    }

    @Test
    public void deveCriarNovoFuncionario() {
        this.funcionarioService.createFuncionario(Funcionario.builder().matricula("1").build());
        assertEquals(1, this.funcionarioService.getAllFuncionarios().size());
    }

    @Test
    public void deveDeletarFuncionario() {
        this.funcionarioService.createFuncionario(Funcionario.builder().matricula("1").build());
        boolean funcionarioDeletado = this.funcionarioService.deleteFuncionario("1");
        assertEquals(true, funcionarioDeletado);
        assertEquals(null, this.funcionarioService.getFuncionarioByMatricula(""));
    }

    @Test
    public void deveDarErroAoDeletarFuncionario() {
        try {
            this.funcionarioService.createFuncionario(Funcionario.builder().build());
            boolean funcionarioDeletado = this.funcionarioService.deleteFuncionario("1");
        } catch (Exception e) {
            assertEquals("class java.lang.NullPointerException", e.getClass().toString());
        }
    }
}

