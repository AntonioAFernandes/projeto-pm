package com.unirio.pm.services;

import com.unirio.pm.domain.Ciclista;
import com.unirio.pm.services.CiclistaService;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CiclistaServiceTest {
    private CiclistaService ciclistaService;

    @Before
    public void setup() {
        this.ciclistaService = new CiclistaService();
    }

    @Test
    public void deveCriarNovoCiclista() {
        this.ciclistaService.createCiclista(Ciclista.builder().id("1").build());
        assertEquals(1, this.ciclistaService.getAllCiclistas().size());
    }

    @Test
    public void deveDeletarCiclista() {
        this.ciclistaService.createCiclista(Ciclista.builder().id("1").build());
        boolean ciclistaDeletado = this.ciclistaService.deleteCiclista("1");
        assertEquals(true, ciclistaDeletado);
        assertEquals(null, this.ciclistaService.getCiclistaById(""));
    }

    @Test
    public void deveDarErroAoDeletarCiclista() {
        try {
            this.ciclistaService.createCiclista(Ciclista.builder().build());
            boolean ciclistaDeletado = this.ciclistaService.deleteCiclista("1");
        } catch (Exception e) {
            assertEquals("class java.lang.NullPointerException", e.getClass().toString());
        }
    }

}

