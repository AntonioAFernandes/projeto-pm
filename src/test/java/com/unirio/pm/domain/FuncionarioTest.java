package com.unirio.pm.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

public class FuncionarioTest {

    Funcionario funcionario = new Funcionario();

    @BeforeEach
    public void init() {
        funcionario.setMatricula("123");
        funcionario.setNome("a");
        funcionario.setCpf("123");
        funcionario.setSenha("a");
        funcionario.setIdade(25);
        funcionario.setEmail("a@a.com");
        funcionario.setFuncao("a");
    }

    @Test
    void testGetMatricula() {
        String matricula = funcionario.getMatricula();
        assertEquals("123", matricula);
    }
    @Test
    void testGetNome() {
        String nome = funcionario.getNome();
        assertEquals("a", nome);
    }
    @Test
    void testGetCpf() {
        String cpf = funcionario.getCpf();
        assertEquals("123", cpf);
    }
    @Test
    void testGetSenha() {
        String senha = funcionario.getSenha();
        assertEquals("a", senha);
    }
    @Test
    void testGetIdade() {
        int idade = funcionario.getIdade();
        assertEquals(25, idade);
    }
    @Test
    void testGetEmail() {
        String email = funcionario.getEmail();
        assertEquals("a@a.com", email);
    }
    @Test
    void testGetFuncao() {
        String funcao = funcionario.getFuncao();
        assertEquals("a", funcao);
    }

}
