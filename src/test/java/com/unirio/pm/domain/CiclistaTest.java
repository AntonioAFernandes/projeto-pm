package com.unirio.pm.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

public class CiclistaTest {

    Ciclista ciclista = new Ciclista();

    @BeforeEach
    public void init() {
        ciclista.setId("123");
        ciclista.setNome("a");
        ciclista.setCpf("12345");
        ciclista.setEmail("a@a.com");
        ciclista.setSenha("123");
        ciclista.setNacionalidade(Nacionalidade.BRASILEIRO);
        ciclista.setUsandoBicicleta(true);
        ciclista.setNascimento("12-12-1212");
        ciclista.setStatus(CiclistaStatus.ATIVO);
        ciclista.setIndicadorBr(true);
        ciclista.setPassaporte(Passaporte.builder().numero("123").validade("12-12-1212").pais("Brasil").build());
        ciclista.setCartao(Cartao.builder().idCartao("123").validade("12-12-1212").cvv("123").numero("12345").nomeTitular("a").build());
    }

    @Test
    void testGetId() {
        String id = ciclista.getId();
        assertEquals("123", id);
    }
    @Test
    void testNome() {
        String nome = ciclista.getNome();
        assertEquals("a", nome);
    }
    @Test
    void testCpf() {
        String cpf = ciclista.getCpf();
        assertEquals("12345", cpf);
    }
    @Test
    void testEmail() {
        String email = ciclista.getEmail();
        assertEquals("a@a.com", email);
    }
    @Test
    void getSenha() {
        String senha = ciclista.getSenha();
        assertEquals("123", senha);
    }
    @Test
    void getNacionalidade() {
        Nacionalidade nacionalidade = ciclista.getNacionalidade();
        assertEquals(Nacionalidade.BRASILEIRO, nacionalidade);
    }
    @Test
    void getNascimento() {
        String nascimento = ciclista.getNascimento();
        assertEquals("12-12-1212", nascimento);
    }
    @Test
    void getStatus() {
        CiclistaStatus status = ciclista.getStatus();
        assertEquals(CiclistaStatus.ATIVO, status);
    }
    @Test
    void getPassaporteNumero() {
        String passaporteNum = ciclista.getPassaporte().getNumero();
        assertEquals("123", passaporteNum);
    }
    @Test
    void getPassaportePais() {
        String passaportePais = ciclista.getPassaporte().getPais();
        assertEquals("Brasil", passaportePais);
    }
    @Test
    void getPassaporteValidade() {
        String passaporteValidade = ciclista.getPassaporte().getValidade();
        assertEquals("12-12-1212", passaporteValidade);
    }
    @Test
    void getUsandoBicicleta() {
        boolean usandoBicicleta = ciclista.isUsandoBicicleta();
        assertEquals(true, usandoBicicleta);
    }
    @Test
    void getIndicadorBr() {
        boolean indicadorBr = ciclista.isIndicadorBr();
        assertEquals(true, indicadorBr);
    }
    @Test
    void getCartaoId() {
        String id = ciclista.getCartao().getIdCartao();
        assertEquals("123", id);
    }
    @Test
    void getCartaoValidade() {
        String validade = ciclista.getCartao().getValidade();
        assertEquals("12-12-1212", validade);
    }
    @Test
    void getCartaoCvv() {
        String cvv = ciclista.getCartao().getCvv();
        assertEquals("123", cvv);
    }
    @Test
    void getCartaoNumero() {
        String numero = ciclista.getCartao().getNumero();
        assertEquals("12345", numero);
    }
    @Test
    void getCartaoTitular() {
        String titular = ciclista.getCartao().getNomeTitular();
        assertEquals("a", titular);
    }
}
