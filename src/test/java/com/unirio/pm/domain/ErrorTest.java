package com.unirio.pm.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ErrorTest {

    Error error = new Error();

    @BeforeEach
    public void init() {
        error.setCodigo("404");
        error.setId("NOT_FOUND_MESSAGE");
        error.setMensagem("Não encontrado");
    }

    @Test
    public void testId() {
        String id = error.getId();
        assertEquals("NOT_FOUND_MESSAGE", id);
    }

    @Test
    public void testCodigo() {
        String codigo = error.getCodigo();
        assertEquals("404", codigo);
    }

    @Test
    public void testMensagem() {
        String mensagem = error.getMensagem();
        assertEquals("Não encontrado", mensagem);
    }
}
