# Microserviço de Aluguel

Microserviço responsável pelas operações de aluguel. Esse microserviço possui duas rotas `HTTP`, sendo elas:

## Get Ciclista (/ciclista):
Operação `GET` que pega um ciclista baseado no id.

### Código HTTP:

Esse método possui três códigos de retorno possíveis, sendo eles:

	1. 200: search results match criteria
	2. 404: not found
    3. 422: bad input parameters

## Post Ciclista (/ciclista):
Operação `POST` que cadastra um novo ciclista no sistema. Recebe um objeto do tipo:

```
{
  "id": "d290f1ee-6c54-4b01-90e6-d701748f0851",
  "email": "joaosilva@email.com",
  "nome": "João Silva",
  "senha": "Joao1234!",
  "indicadorBr": true,
  "usandoBicicleta": true,
  "bicicletaAtiva": {
    "id": "d290f1ee-6c54-4b01-90e6-d701748f0851",
    "codigo": 90851234678,
    "emUso": false
  },
  "documento": {
    "tipo": "CPF",
    "numero": 15954068715,
    "foto": "foto.jpg"
  },
  "cartão": {
    "numero": 1234567890123456,
    "proprietario": "JOAO SILVA",
    "validade": "01/20",
    "codigo": 123
  }
}
```

### Código HTTP:

Esse método possui dois códigos de retorno possíveis, sendo eles:

	1. 201: item created
	2. 405: invalid data

## Put Ciclista (/ciclista):
Operação `PUT` que altera os dados de um ciclista existente baseado no seu id.

### Código HTTP:
Esse método possui três códigos de retorno possíveis, sendo eles:

    1. 200: data updated
    2. 404: not found
    3. 422: bad input parameters

## Post Ativar (/ciclista/{id}/ativar)
Operação `POST` que altera o status da ativação do ciclista para `ATIVO` através do ID do ciclista.

### Código HTTP:
Esse método possui três códigos de retorno possíveis, sendo eles:

    1. 200: data updated
    2. 404: not found
    3. 405: invalid data

## Get existeEmail (/ciclista/existeEmail)
Operação `GET` que verifica se um email já está cadastrado no sistema por algum ciclista.

### Código HTTP:
Esse método possui três códigos de retorno possíveis, sendo eles:

    1. 200: true if email exists; false if not
    2. 400: email not parameter
    3. 422: invalid data

## Post Funcionários (/funcionarios)
Operação `POST` que cadastra um novo funcionário no sistema. Recebe um objeto do tipo:
```
{
    "senha": "string",
    "email": "user@example.com",
    "nome": "string",
    "idade": int,
    "funcao": "string",
    "cpf": "string"
}
```

### Código HTTP:
Esse método possui dois códigos de retorno possíveis, sendo eles:

	1. 201: item created
	2. 405: invalid data

## Get Todos Funcionários (/funcionarios)
Operação `GET` que retorna todos os funcionários cadastrados.

### Código HTTP:
Esse método possui apenas um código de retorno:
    
    1. 200: OK

## Get Funcionário (/funcionario)
Operação `GET` que pega um funcionário baseado na sua matrícula.

### Código HTTP:

Esse método possui três códigos de retorno possíveis, sendo eles:

	1. 200: search results match criteria
	2. 404: not found
    3. 422: bad input parameters

## Put Funcionário (/funcionario)
Operação `PUT` que altera os dados de um funcionário existente baseado na sua matrícula.

### Código HTTP:
Esse método possui três códigos de retorno possíveis, sendo eles:

    1. 200: data updated
    2. 404: not found
    3. 422: bad input parameters

## Delete Funcionário (/funcionario)
Operação `DELETE` que remove um funcionário da lista de funcionários do serviço.

### Código HTTP:
Esse método possui três códigos de retorno possíveis, sendo eles:

    1. 200: data removed
    2. 404: not found
    3. 422: invalid data

## Get Cartão de Crédito (/cartaoDeCredito)
Operação `GET` que pega os dados do cartão de crédito de um ciclista usando o seu id.

### Código HTTP: 
Esse método possui três códigos de retorno possíveis, sendo eles:

    1. 200: data retrieved
    2. 404: not found
    3. 422: invalid data

## Put Cartão de Crédito (/cartaoDeCredito)
Operação `PUT` que altera os dados do cartão de crédito de um ciclista baseado no seu id.

### Código HTTP:
Esse método possui três códigos de retorno possíveis, sendo eles:

    1. 200: data updated
    2. 404: not found
    3. 422: invalid data

## Post Aluguel (/aluguel)
Operação `POST` que realiza o aluguel de uma tranca para um ciclista, realiza uma cobrança no seu cartão de crédito de um valor fixo e libera a tranca para uso do ciclista, além de notificá-lo sobre a retirada da bicicleta. Recebe um objeto do tipo:
```
{
  "ciclista": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "trancaInicio": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
}
```

### Código HTTP:
Esse método possui dois códigos de retorno possíveis, sendo eles:

    1. 200: slot rented
    2. 422: invalid data

## Post Devolução:
Operação `POST` que realiza a devolução de uma tranca após o uso pelo ciclista. Recebe um objeto do tipo:

```
{
  "idTranca": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "idBicicleta": "3fa85f64-5717-4562-b3fc-2c963f66afa6"
}
```
### Código HTTP:
Esse método possui dois códigos de retorno possíveis, sendo eles:

    1. 200: slot returned
    2. 422: invalid data
